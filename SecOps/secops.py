import re

class Anonymizer:
    def __init__(self, data):
        self.data = data

    def anonymize(self):
        raise NotImplementedError("Subclasses must implement anonymize method")


class EmailAnonymizer(Anonymizer):
    def anonymize(self):
        # Разделение по @
        username, domain = self.data.split('@')
        # Анонимизация имени пользователя (оставляем только первую и последнюю букву, остальные заменяем на '*')
        anonymized_username = username[0] + '*'*(len(username)-2) + username[-1]
        return f"{anonymized_username}@{domain}"


class SkypeAnonymizer(Anonymizer):
    def anonymize(self):
        # Проверка на наличие HTML-тегов
        if '<' in self.data and '>' in self.data:
            # Если есть HTML, оставляем его вокруг анонимизированного имени пользователя
            return re.sub(r'(?<=skype:)[a-zA-Z0-9]+', lambda match: '*'*len(match.group()), self.data)
        else:
            return re.sub(r'[a-zA-Z0-9]+', lambda match: '*'*len(match.group()), self.data)


class PhoneAnonymizer(Anonymizer):
    def anonymize(self):
        # Анонимизация последних 3 цифр
        return re.sub(r'\d{3}(?=\d{3}$)', lambda match: '*'*len(match.group()), self.data)


# Пример использования:
email_data = 'test@example.com'
skype_data = 'skype:username'
phone_data = '+234 777888999'

email_anonymizer = EmailAnonymizer(email_data)
skype_anonymizer = SkypeAnonymizer(skype_data)
phone_anonymizer = PhoneAnonymizer(phone_data)

print("Anonymized email:", email_anonymizer.anonymize())
print("Anonymized Skype username:", skype_anonymizer.anonymize())
print("Anonymized phone number:", phone_anonymizer.anonymize())
