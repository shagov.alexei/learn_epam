#  digits.py: Find sum of n-integer digits. n >= 0.

def sum_int(num):
    if num >= 0:
        result = 0
        for n in str(num):
            result += int(n)
        return result
    else:
        return 0



def sum_of_digits_str(num):
    return sum(int(digit) for digit in str(num))

print(sum_int(0))
print(sum_int(1))
print(sum_int(312))
print(sum_int(1234))

print(sum_of_digits_str(0))
print(sum_of_digits_str(1))
print(sum_of_digits_str(321))
print(sum_of_digits_str(1234))
