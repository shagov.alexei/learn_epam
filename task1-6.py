'''sets.py: Find common items in 2 lists without duplicates. Sort the result list before output.'''
def find_common_sorted(list1, list2):
    # Преобразование списков в множества для удаления дубликатов
    set1 = set(list1)
    set2 = set(list2)

    # Находим пересечение множеств
    common_elements = set1.intersection(set2)

    # Преобразуем множество обратно в список и сортируем его
    result = sorted(list(common_elements))

    return result

# Пример использования
list1 = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
list2 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

# Вызываем функцию и выводим результат
print(find_common_sorted(list1, list2))
