import yaml
import jinja2

def load_data_from_yaml(filename):
    with open(filename, 'r') as file:
        return yaml.safe_load(file)

def render_template(template_path, data):
    template_loader = jinja2.FileSystemLoader(searchpath="./")
    template_env = jinja2.Environment(loader=template_loader)
    template = template_env.get_template(template_path)
    return template.render(data)

def main():
    data = load_data_from_yaml('data.yml')
    output = render_template('vhosts.j2', data)
    with open('vhosts.conf', 'w') as conf_file:
        conf_file.write(output)

if __name__ == "__main__":
    main()
