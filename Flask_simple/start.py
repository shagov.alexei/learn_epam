from flask import Flask, render_template, request, redirect, url_for

app = Flask(__name__)

# Список сообщений пользователей (временное хранилище)
messages = []

@app.route('/')
def index():
    return render_template('index.html', messages=messages)

@app.route('/post_message', methods=['POST'])
def post_message():
    message = request.form['message']
    messages.append(message)
    return redirect(url_for('index'))

if __name__ == '__main__':
    app.run(debug=True)
