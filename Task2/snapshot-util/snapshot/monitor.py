'''
TASK 1
 Create a simple python app which would monitor your system/server. Output should be written to json file and stdout.
 For monitoring purposes use psutil module, see:
https://pypi.org/project/psutil/
 It should create snapshots of the state of the system each 30 seconds (configurable):
 {"Tasks": {"total": 440, "running": 1, "sleeping": 354, "stopped": 1, "zombie": 0},
 "%CPU": {"user": 14.4, "system": 2.2, "idle": 82.7},
 "KiB Mem": {"total": 16280636, "free": 335140, "used": 11621308},
 "KiB Swap": {"total": 16280636, "free": 335140, "used": 11621308},
 "Timestamp": 1624400255}
 Output should be written to the console and json file.
 The script has to accept an interval (default value = 30 seconds) and output file name. Use argparse module, see:
 https://docs.python.org/3/library/argparse.html
 import argparse
 parser = argparse.ArgumentParser()
 parser.add_argument("-i", help="Interval between snapshots in seconds", type=int, default=30)
 parser.add_argument("-f", help="Output file name", default="snapshot.json")
 parser.add_argument("-n", help="Quantity of snapshot to output", default=20)
 args = parser.parse_args()
 ...
 with open((args.f, "a") as file:
 # use json.dump to write JSON-snapshot to file
 # don’t forget to import json before
 ...
 Use for console output: os.system('clear') print(snapshot, end="\r")
 Use time.sleep() to make an interval.
 Timestamp is the current time timestamp without a float part.
 Separate snapshots in json-file by new line.
 Clean file content when the script is started.
 (!) At least one (any) class should be created.
'''

import psutil
import json
import time
import os
import argparse
from datetime import datetime

class SystemMonitor:
    def __init__(self, filename):
        self.filename = filename

    def take_snapshot(self):
        # Gather system statistics using psutil
        cpu_percent = psutil.cpu_percent(interval=None, percpu=False)
        mem = psutil.virtual_memory()
        swap = psutil.swap_memory()
        tasks = {
            'total': len(psutil.pids()),
            'running': len([p for p in psutil.process_iter(attrs=['status']) if p.info['status'] == 'running']),
            'sleeping': len([p for p in psutil.process_iter(attrs=['status']) if p.info['status'] == 'sleeping']),
            'stopped': len([p for p in psutil.process_iter(attrs=['status']) if p.info['status'] == 'stopped']),
            'zombie': len([p for p in psutil.process_iter(attrs=['status']) if p.info['status'] == 'zombie'])
        }

        snapshot = {
            "Tasks": tasks,
            "%CPU": {
                "user": cpu_percent
            },
            "KiB Mem": {
                "total": mem.total,
                "free": mem.free,
                "used": mem.used
            },
            "KiB Swap": {
                "total": swap.total,
                "free": swap.free,
                "used": swap.used
            },
            "Timestamp": int(datetime.now().timestamp())
        }
        return snapshot

    def write_to_file(self, data):
        with open(self.filename, "a") as file:
            json.dump(data, file)
            file.write('\n')

def main():
    # Set up command-line argument parsing
    parser = argparse.ArgumentParser(description="Monitor system and log snapshots to JSON file.")
    parser.add_argument("-i", "--interval", help="Interval between snapshots in seconds", type=int, default=10)
    parser.add_argument("-f", "--file", help="Output file name", default="snapshot.json")
    parser.add_argument("-n", "--number", help="Quantity of snapshot to output", type=int, default=2)
    args = parser.parse_args()

    monitor = SystemMonitor(args.file)

    # Clear file content when the script is started
    open(args.file, 'w').close()

    # Take snapshots at the specified interval and write to both stdout and file
    for _ in range(args.number):
        os.system('clear')  # Clear console
        snapshot = monitor.take_snapshot()
        print(json.dumps(snapshot, indent=4), end="\r")  # Print to console
        monitor.write_to_file(snapshot)  # Write to file
        time.sleep(args.interval)

if __name__ == "__main__":
    main()
