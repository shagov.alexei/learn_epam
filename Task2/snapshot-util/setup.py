from setuptools import setup, find_packages

setup(
    name="snapshot",
    version="0.1.0",
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'snapshot=snapshot.monitor:main'  # Позволяет выполнение из командной строки
        ]
    },
    author="Alex Sha",
    author_email="shagov.alexei@gmail.com",
    description="Инструмент для мониторинга системы и регистрации снимков.",
    long_description=open('README.md').read(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/shagov.alexei/learn_epam/snapshot-util",
    install_requires=[
        "psutil>=5.7.0",
    ],
    python_requires='>=3.6',
)
