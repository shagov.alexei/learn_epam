# strings.py: Check whether the input string is a palindrome.

def palindrom(str):
    filtered_chars = [char.lower() for char in str if char.isalnum()]
    filtered_string = ''.join(filtered_chars)
    if filtered_string == filtered_string[::-1]:
        return 'yes'
    else:
        return 'no'


def check_polindrame(s):
    rev_s = s[::-1]
    if s == rev_s:
        return 'yes'
    return 'no'

print(palindrom('abcba'))
print(palindrom('test'))

print(check_polindrame('abcba'))
print(check_polindrame('test'))
