# loop.py: Calculate n!. n! = 1 * 2 * 3 * … * (n-1) * n, 0! = 1. n >= 0.

def factorial_iterative(n):
    if n == 0:
        return 1
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result

def factorial_recursive(n):
    if n == 0:
        return 1
    return n * factorial_recursive(n - 1)

# Тестируем итеративный метод
print(factorial_iterative(3))
print(factorial_iterative(5))

# Тестируем рекурсивный метод
print(factorial_recursive(3))
print(factorial_recursive(5))
