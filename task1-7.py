'''
split_join.py (add by yourself): Given a string, you need to reverse the order of characters in each word within a sentence while still preserving
 whitespace and initial word order.
 In the string, each word is separated by single space and there will not be any extra space in the string.
 Example:
 Input:
 s'teL ekat edoCteeL tsetnoc

'''
def reverse_words_in_sentence(sentence):
    # Разбиваем строку на слова по пробелу
    words = sentence.split(' ')

    # Реверсируем каждое слово и возвращаем результат
    reversed_words = [word[::-1] for word in words]

    # Соединяем слова обратно в строку с пробелами
    reversed_sentence = ' '.join(reversed_words)

    return reversed_sentence

# Пример использования
input_sentence = "Let's take LeetCode contest"
output_sentence = reverse_words_in_sentence(input_sentence)
print(output_sentence)
