import os
import requests

TOKEN = os.getenv("TOKEN")
HEADERS = {'Authorization': f'Bearer {TOKEN}'}

def get_pull_requests(state):
    url = f'https://api.github.com/repos/boto/boto3/pulls?state={state}&per_page=100'
    response = requests.get(url, headers=HEADERS)
    if response.status_code == 200:
        data = response.json()
        pull_requests = [{"num": pr["number"], "title": pr["title"], "link": pr["html_url"]} for pr in data]
        return pull_requests
    else:
        return []
