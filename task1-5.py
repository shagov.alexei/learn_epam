'''dicts.py: Drop empty items from a dictionary.'''

def drop_empty_items(dic):
    # Использование генератора словарей, который включает элементы, где значение не является "пустым"
    return {k: v for k, v in dic.items() if v not in [None, "", [], (), {}]}

# Пример использования
original_dict = {
    "a": None,
    "b": "",
    "c": [1, 2],
    "d": [],
    "e": {},
    "f": 0,
    "g": "text",
    "h": (1, 2),
    "i": ()
}

# Вызов функции
cleaned_dict = drop_empty_items(original_dict)

# Вывод результата
print(cleaned_dict)
